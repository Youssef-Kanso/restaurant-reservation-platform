var myIndex = 0;
carousel();

function carousel() {
    var i;
    var Slide = document.getElementsByClassName("mySlides");
    for (i = 0; i < Slide.length; i++) {
       Slide[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > Slide.length) {myIndex = 1}    
    Slide[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2000);    
}

$(document).ready(function(){
  // Add scrollspy to <body>
  $('body').scrollspy({target: ".navbar", offset: 50});   

  // Add smooth scrolling on all links inside the navbar
  $("#myNavbar a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    }  // End if
  });
}); 