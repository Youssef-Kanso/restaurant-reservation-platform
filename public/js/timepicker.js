_f = function(id){
  return document.getElementById(id);
}

var mylib = {};
mylib.calendar = {};
mylib.calendar.months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
mylib.calendar.weekdays = ["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat"];
mylib.calendar.currentDate = new Date();

mylib.calendar.setTimeLabels = function(){
  var roundedTime = this.getRoundedTime();
  
  _f("hour").innerText = roundedTime.hours;
  _f("minutes").innerText = roundedTime.minutes;
  _f("ampm").innerText = roundedTime.ampm;
  _f("titleT").innerText = roundedTime.hours + ":" + roundedTime.minutes + " " + roundedTime.ampm;
}

mylib.calendar.getRoundedTime = function(){
  var retObj = {};
  retObj.hours = this.currentDate.getHours();
  var minutes = this.currentDate.getMinutes();
  if(minutes < 15){
    retObj.minutes = 15;
  } else if(minutes < 30){
    retObj.minutes = 30;
  } else if(minutes < 45){
    retObj.minutes = 45;
  } else {
    retObj.minutes = 0;
    retObj.hours = retObj.hours + 1;
  }
   
  retObj.ampm = retObj.hours >= 12 ? "PM" : "AM";
  
  if(retObj.hours > 12) retObj.hours = retObj.hours - 12;
  if(retObj.hours < 10) retObj.hours = "0"+retObj.hours;  
  
  if(retObj.minutes < 10) retObj.minutes = "0"+retObj.minutes;
  
  return retObj;
}

mylib.calendar.setTimeLabels();

addHour = function(numberOfHours){
  var oldDate = mylib.calendar.currentDate;
  mylib.calendar.currentDate.setHours(oldDate.getHours() + numberOfHours);
  mylib.calendar.setTimeLabels();
}

addMinutes = function(numberOfMinutes){
  var oldDate = mylib.calendar.currentDate;
  mylib.calendar.currentDate.setMinutes(oldDate.getMinutes() + numberOfMinutes);
  mylib.calendar.setTimeLabels();
}

toggleAMPM = function(){
  if(_f("ampm").innerText == "AM") {
    _f("ampm").innerText = "PM";
  } else {
    _f("ampm").innerText = "AM";
  }
}