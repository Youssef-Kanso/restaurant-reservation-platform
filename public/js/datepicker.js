_d = function(id){
  return document.getElementById(id);
}

var mylib = {};
mylib.calendar = {};
mylib.calendar.months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
mylib.calendar.weekdays = ["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat"];
mylib.calendar.currentDate = new Date();

mylib.calendar.setDateLabels = function(){
  /*console.log(sessionStorage.getItem("date"));
  if(sessionStorage.getItem("date")){ 
    console.log("1");
    var d = sessionStorage.getItem("date");
  } else {
    console.log("2");
 
  }*/
     var d = this.currentDate;
  _d("day").innerText = d.getDate();
  _d("month").innerText = this.months[d.getMonth()];
  _d("year").innerText = d.getFullYear();
  _d("title").innerText = this.weekdays[d.getDay()] + ", " + this.months[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
  _d("date").value = d.getFullYear() + "-" +("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
}

mylib.calendar.setDateLabels();

addDay = function(numberOfDays){
  var oldDate = mylib.calendar.currentDate;
  mylib.calendar.currentDate.setDate(oldDate.getDate() + numberOfDays);
  mylib.calendar.setDateLabels();
}

// TODO: Use addDay() function for adding month
addMonth = function(numberOfMonths){
  var oldDate = mylib.calendar.currentDate;
  mylib.calendar.currentDate.setMonth(oldDate.getMonth() + numberOfMonths);
  mylib.calendar.setDateLabels();
}

// TODO: Use addDay() function for adding Year
addYear = function(numberOfYears){
  var oldDate = mylib.calendar.currentDate;
  mylib.calendar.currentDate.setFullYear(oldDate.getFullYear() + numberOfYears);
  mylib.calendar.setDateLabels();
}


_f = function(id){
  return document.getElementById(id);
}

/*var mylib = {};
mylib.calendar = {};
mylib.calendar.months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
mylib.calendar.weekdays = ["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat"];
mylib.calendar.currentDate = new Date();*/

mylib.calendar.setTimeLabels = function(){
  var roundedTime = this.getRoundedTime();
  var t = this.currentDate;
  _f("hour").innerText = roundedTime.hours;
  _f("minutes").innerText = roundedTime.minutes;
  _f("ampm").innerText = roundedTime.ampm;
  _f("titleT").innerText =roundedTime.hours + ":" + roundedTime.minutes + " " + roundedTime.ampm;
  _f("time").value =roundedTime.hours + ":" + roundedTime.minutes ;
}

mylib.calendar.getRoundedTime = function(){
  var retObj = {};
  retObj.hours = this.currentDate.getHours();
  var minutes = this.currentDate.getMinutes();
  if(minutes < 15){
    retObj.minutes = 15;
  } else if(minutes < 30){
    retObj.minutes = 30;
  } else if(minutes < 45){
    retObj.minutes = 45;
  } else {
    retObj.minutes = 0;
    retObj.hours = retObj.hours + 1;
  }
   
  retObj.ampm = retObj.hours >= 12 ? "PM" : "AM";
  
  if(retObj.hours > 12) retObj.hours = retObj.hours - 12;
  if(retObj.hours < 10) retObj.hours = "0"+retObj.hours;  
  
  if(retObj.minutes < 10) retObj.minutes = "0"+retObj.minutes;
  
  return retObj;
}

mylib.calendar.setTimeLabels();

addHour = function(numberOfHours){
  var oldDate = mylib.calendar.currentDate;
  mylib.calendar.currentDate.setHours(oldDate.getHours() + numberOfHours);
  mylib.calendar.setTimeLabels();
}

addMinutes = function(numberOfMinutes){
  var oldDate = mylib.calendar.currentDate;
  mylib.calendar.currentDate.setMinutes(oldDate.getMinutes() + numberOfMinutes);
  mylib.calendar.setTimeLabels();
}

toggleAMPM = function(){
  if(_f("ampm").innerText == "AM") {
    _f("ampm").innerText = "PM";
  } else {
    _f("ampm").innerText = "AM";
  }
}
/*function event(){
  console.log("batat");
  var button = document.getElementById("checkReservation");
  button.addEventListener("click",function() {setSession();} );
}

function setSession() {
  console.log("bl");
  var d = document.getElementById("date");
  console.log(d);
  sessionStorage.setItem("date", d);
  var t =document.getElementById("time");
  sessionStorage.setItem("time", t);
}

event();
*/

