<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/menu', 'MenuController@index')->name('menu');

Route::get('/contact', 'ContactController@index')->name('contact');

Route::get('/events', 'ChefController@index')->name('events');

Route::get('/reservation', 'ReservationController@index')->name('reservation');

Route::post('/Reservation', "ReservationController@make")->name('Reservation');

Route::post('/makeReservation', 'ReservationController@store')->name('reserved');

Route::post('/order', 'OrderController@store')->name('order');

Route::get('/availablity', 'AvailablityController@index')->name('availablity');

Route::post('/checkAvailablity', 'AvailablityController@check')->name('checkAvailablity');

Route::post('/reservations', 'ReservationController@update')->name('reservations1');

Route::get('/reservations', 'ReservationController@admin')->name('reservations');

Route::get('/myResevations', 'ReservationController@get')->name('myResevations');