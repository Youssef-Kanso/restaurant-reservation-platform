@extends('layouts.app')
@section('content')
<div class="container" style="margin-top: 100px;">
	<div class="col-md-10 col-md-offset-1">
		<table  class="table table-bordered" id="mytable">
			<thead >
				<tr class="success">
					<th>Table Number</th>
					<td>Date</td>
					<th>Time</th>
					<th>Created</th>
				</tr>
			</thead>
			@foreach($result as $reservation)
			<tr>
				<td>{{ $reservation->table_number }}</td>
				<td>{{ $reservation->date }}</td>
				<td>{{ $reservation->time }}</td>
				<td>{{ $reservation->created_at }}</td>
			</tr>
			@endforeach
		</table>
	</div>
</div>

@endsection