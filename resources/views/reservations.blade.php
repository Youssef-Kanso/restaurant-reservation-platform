@extends('layouts.app')
@section('content')
<div class="container" style="margin-top: 100px;">
	<div class="col-md-10 col-md-offset-1">
		<table  class="table table-bordered" id="mytable">
			<thead >
				<tr class="success">
					<th>Name</th>
					<th>Phone Number</th>
					<td>Date</td>
					<th>Time</th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			@foreach($res as $reservation)
			<tr>
				<td>{{ $reservation->user->name }}</td>
				<td>{{ $reservation->user->phone_number }}</td>
				<td>{{ $reservation->date }}</td>
				<td>{{ $reservation->time }}</td>
				<td>@if($reservation->end_reservation == 1)
						available 
					@else
						reserved 
					@endif
				</td>
				<td>
						<form method="POST" action="{{URL::to('/')}}/reservations" style="width:20%;">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{$reservation->table_number}}">
						<input type="submit" value="Available"></button>
					</form>
				</td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection