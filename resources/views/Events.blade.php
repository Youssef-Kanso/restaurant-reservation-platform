@extends('layouts.app')
@section('content')

        <!-- Carousel -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000"
    style="margin-bottom: 20px; width: 100%;">

       <!-- Wrapper for slides -->
        <div class="carousel-inner" style="width: 100%">
            <div class="item active">     
                
                    <img src="img/i9.jpg" style="width:100%; height: 631px;">
                    
                
            </div>
            <div class="item">
            	<img src="img/i6.jpg" style="width:100%; height: 631px;">
                    
            </div>
            <div class="item">
            <img src="img/i10.jpg" style="width:100%; height: 631px;">
            </div>
       <!-- Left and right controls -->
        	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
            	<span class="glyphicon glyphicon-chevron-left" style="opacity: 0.3"></span>
            	<span class="sr-only">Previous</span>
        	</a>
       		<a class="right carousel-control" href="#myCarousel" data-slide="next">
            	<span class="glyphicon glyphicon-chevron-right" style="opacity: 0.3"></span>
            	<span class="sr-only">Next</span>
        	</a>
        </div>
    </div>
 	

 	<section>
 		<div class="container">
 			<div class="col-md-12 col-md" style="padding: 0;">
	        	<p class="col-md-offset-2" style="font-size:20pt;  color:#363636">LATEST NEWS <a href="#" class="col-md-offset-7">View all news </a></p>
		                    
	        	<div class="row" style=" padding-bottom: 40px; margin-top: 10px;">
		            <div class="news_left col-md-12" style="padding: 0">
		            	<div class="col-md-4 col-md-offset-2" ><img src="img/KidsArea.jpg" /></div>
		                <div class="col-md-4">
		                	<h2> Kids area opening</h2>
		                    <div class="desctxt">
			                    Soon at the Bridge restaurant, parents and friends can come and relax while the children can safely play in the clean, spacious and entertaining childrens fun centre.  
								At the specially...<br /><br />
			                	<a href="#">Read more </a><br/>
		                    </div>
		                </div>
		                <br clear="all" />
		            </div>
		        </div>

		        <div class="row col-md-4 col-md-offset-4" style="border-top: 2px solid black;"></div>


	        	<div class="row" style="padding-bottom: 40px; margin-top: 25px; padding: 0; ">
		            <div class="col-md-12" style="padding: 0">
		            	<a href="#">
			                <div class="col-md-4 col-md-offset-2">
			                	<div class="vid"></div>
			                    <img src="http://img.youtube.com/vi/ZsOuTFmFxQs/1.jpg" width="320px" height="180px" alt="" title="" />
			                </div>
			                <div class="col-md-4">
			                	<h2>Latest TVC</h2>
			                    <div class="desctxt">In this section you will find the latest TVC. Please keep on visiting our page for latest updates </div>
		               		</div>
		                </a>
		              	<br clear="all" />
	            	</div>
		         </div>
	         	<br>
		    </div>
    	</div>
	</section>    
        
    <div class="footer">
    

    <style>.ig-b- { display: inline-block; }
.ig-b- img { visibility: hidden; }
.ig-b-:hover { background-position: 0 -60px; } .ig-b-:active { background-position: 0 -120px; }
.ig-b-48 { width: 48px; height: 48px; background: url(//badges.instagram.com/static/images/ig-badge-sprite-48.png) no-repeat 0 0; }
@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
.ig-b-48 { background-image: url(//badges.instagram.com/static/images/ig-badge-sprite-48@2x.png); background-size: 60px 178px; } }</style>
        
    
    
</div>




@endsection