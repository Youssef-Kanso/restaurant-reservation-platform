@extends('layouts.app')

@section('content')

        <div id="homePage">
            <img src="img/bridge.svg" id="B" alt="A"/>
            <div class="caption" style="background-color:rgba(0, 0, 0, 0.8);" >
            	The highest quality cuisine combined with the best service in a welcoming and friendly atmosphere
            </div>
        </div>
    
@endsection
