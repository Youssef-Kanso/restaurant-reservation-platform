@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="col-md-6" style="margin-top: 65px;">
			<img src="{{ asset('img/tableReservation.jpg') }}" style="width: 100%;height: 100%;">
		</div>
		<div class="col-md-6" style="width: 40%;  margin-top: 5%;">
			<form method="POST" action="{{URL::to('/')}}/makeReservation">
				{{ csrf_field() }}

				<input type="date" min='{{  date("Y-m-d") }}' value="{{ $date }}" disabled="disabled" class="input" style="width: 100%; ">
				<input type="time" value="{{ $time }}" disabled="disabled" class="input" style=" width: 39%">
				<input type="text" value="{{ $table_n }}" class="input" disabled="disabled">
				
				<input type="hidden" name="date" value="{{ $date }}">
				<input type="hidden" name="time" value="{{ $time }}">
				<input type="hidden" name="table_number" value="{{ $table_n }}">
				
				<div class="col-md-12 btn">
					<button type="submit" class="btn btn-success" style="margin-top: 20px;">Reserve</button>
				</div>
			</form>
			
		</div>
	</div>
@endsection
