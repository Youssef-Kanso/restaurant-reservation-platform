@extends('layouts.app')
@section('content')
    <nav class="navbar navbar-default navbar-fixed-top" style="margin-top: 50px; opacity: 0.8;">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand colorBlack" href="#">Bridge</a>
            </div>
             <div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="dropdown"><a class="dropdown-toggle colorBlack" data-toggle="dropdown" href="#">Appetizers <span class="caret"></span></a>
                            <ul class="dropdown-menu colorBlack">
                                <li><a class="colorBlack" href="#Soups">Soups</a></li>
                                <li><a class="colorBlack" href="#Starters">Starters</a></li>
                            </ul>
                        </li>

                        <li ><a class="colorBlack" href="#Salads">Salads </a></li>
                                
                        <li class="dropdown"><a class="dropdown-toggle colorBlack" data-toggle="dropdown" href="#">Fast Food<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="colorBlack" href="#FastFood">Fast Food</a></li>
                                <li><a class="colorBlack" href="#Pizza">Pizza</a></li>
                            </ul>
                        </li>

                        <li class="dropdown"><a class="dropdown-toggle colorBlack" data-toggle="dropdown" href="#">Main Course<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="colorBlack" href="#SeaFood">Fish & SeaFood</a></li>
                                <li><a class="colorBlack" href="#BeefandPoultry">Beef and Poulet</a></li>
                            </ul>
                        </li>

                        <li ><a class="colorBlack" href="#ItalianCorner">Italian </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>  
    <!-- Begin Section Slider -->
    <section>   
    	<div class="container">
    		<div id="menuPage" style="margin-top: 100px;">
    			<!-- Slideshow container -->
                <div class="slideshow-container w3-content w3-section">
                    <div class="w3-content w3-section"  >
                        <img class="mySlides w3-animate-fading images" src="img/menu16.jpg" >
                        <img class="mySlides w3-animate-fading images" src="img/menu13.jpg" >
                        <img class="mySlides w3-animate-fading images" src="img/menu14.jpg" >
                        <img class="mySlides w3-animate-fading images" src="img/menu15.jpg" >
                        <img class="mySlides w3-animate-fading images" src="img/menu11.jpg" >
                    </div>
                </div>
    	   </div>
        </div>
    </section>
    <!-- End Section -->
    
    <!-- Begin Section 1 Menu -->
    <section class="sections" id="Soups" >
        <div class="col-md-4 div-left"  >
            <img src="img/soups.jpg" class="menuImages">
        </div>
        <div class="col-md-4 div-left">
            <p style="font-size: 40px; text-align: center;"><i><b>Soups</b></i></p>
            
            <p align="center" style="font-size: 20px;"><b>Lentil Soup  8 000 l.l</b></p>
            <p align="center">crushed lentil, onion, celery, coriander,<br> 
            served with croutons and lemon wedges </p>

            <p align="center" style="font-size: 20px;"><b>Creamy Soup  10 000 l.l</b></p>
            <p align="center">your choice of asparagus, brocoli or mashroom </p>

            <p align="center" style="font-size: 20px;"><b>Onion Soup  10 000 l.l</b></p>
            <p align="center">caramelized onion with beef stock<br> 
            and melted cheese on top</p>
        </div>
    </section>
    <!-- End Section -->

    <!-- Begin Section 2 Menu -->
    <section class="sections" id="Starters">
        
        <div class="col-md-4 div-right" >
            <img src="img/starters1.jpg" class="menuImages">
        </div>
        <div class="col-md-4 div-right" >
            <p style="font-size: 40px; text-align: center;" ><i><b>starters</b></i></p>
            
            <p align="center" style="font-size: 20px;"><b>Smoked Salamon  31 500 l.l</b></p>
            <p align="center">salamon with brown baguette<br> 
            served with capers, lemon wedges amd cream cheese</p>

            <p align="center" style="font-size: 20px;"><b>Mozarella Sticks  9 500 l.l</b></p>
            <p align="center">special breaded mozzarella cheese<br> 
            served with cocktail sauce </p>

            <p align="center" style="font-size: 20px;"><b>Potato Wedges  7 000 l.l</b></p>
            <p align="center"> served with cocktail sauce </p>

            <p align="center" style="font-size: 20px;"><b>Chicken Crispy  14 000 l.l</b></p>
            <p align="center">breaded chicken strips<br> 
            served with fries, coleslaw and cocktail sauce</p>

            <p align="center" style="font-size: 20px;"><b>Shrimp Crispy  15 000 l.l</b></p>
            <p align="center">served with french fries and cocktail sauce</p>

            <p align="center" style="font-size: 20px;"><b>Onion Rings  7 000 l.l</b></p>
            <p align="center">served with cheader cheese</p>
        </div>
    </section>
    <!-- End Section -->


    <!-- Begin Section 3 Menu -->
    <section class="sections" id="Salads">
        <div class="col-md-4 div-left">
            <img src="img/salads.jpg" class="menuImages">
        </div>
        <div class="col-md-4 div-left">
            <p style="font-size: 40px; text-align: center;"><i><b>Salads</b></i></p>
            
            <p align="center" style="font-size: 20px;"><b>Maxicn salad  15 000 l.l</b></p>
            <p align="center">sweet corn, black olive, avocado, red onion,<br> 
            tomato, green peper, cucumber<br> 
            served with house dressing</p>

            <p align="center" style="font-size: 20px;"><b>Italian Chicken Salad  15 000 l.l</b></p>
            <p align="center">marinated chicken breast, iceberg lettuce,<br>
            tomato, cucmber, green onion<br> 
            served with house dressing</p>

            <p align="center" style="font-size: 20px;"><b>Green Salad  12 000 l.l</b></p>
            <p align="center">mixed lettuce, cucumber,<br> 
            served with lemon dressing</p>

            <p align="center" style="font-size: 20px;"><b>Chicken Caesar Salad  18 000 l.l</b></p>
            <p align="center">roman lettuce, grilled chicken strips,croutons, permesan<br> 
            served with ceaser dressing</p>

            <p align="center" style="font-size: 20px;"><b>Panache Salad  19 250 l.l</b></p>
            <p align="center">palmito, asparagus, artichoke, cherry tomato,<br>
            assorted lettuce, bay corn, avocado<br> 
            served with lemon mayo dressing</p>

            <p align="center" style="font-size: 20px;"><b>Greek Salad  14 500 l.l</b></p>
            <p align="center">mixed lettuce, fetta cheese, cherry tomato, cucumber, thyme, onion<br> 
            served with lemon mint dressing</p>
        </div>
    </section>
    <!-- End Section -->



      <!-- Begin Section 4 Menu -->
    <section class="sections" id="FastFood">
        <div class="col-md-4 div-right" >
            <img src="img/fastfood.jpg" class="menuImages">
        </div>
        <div class="col-md-4 div-right">
            <p style="font-size: 40px; text-align: center;"><i><b>Fast Food</b></i></p>
            
            <p align="center" style="font-size: 20px;"><b>Chicken Nuggets  12 500 l.l</b></p>
            <p align="center">crhcken nuggets served with<br> 
            coleslaw, french fries and tomatoes </p>

            <p align="center" style="font-size: 20px;"><b>Chicken Escalope  16 000 l.l</b></p>
            <p align="center">served withcoleslaw and french fries </p>

            <p align="center" style="font-size: 20px;"><b>Burger  15 000 l.l</b></p>
            <p align="center">beef burger, served with<br> 
            coleslaw french fries and tomatoes</p>
    </section>
    <!-- End Section -->

    <!-- Begin Section 3 Menu -->
    <section class="sections"  id="Pizza">
        <div class="col-md-4 div-left">
            <img src="img/mini.jpg" class="menuImages">
        </div>
        <div class="col-md-4 div-left">
            <p style="font-size: 40px; text-align: center;"><i><b>Pizza</b></i></p>
            
            <p align="center" style="font-size: 20px;"><b>Margeritta Pizza  12 000 l.l</b></p>

            <p align="center" style="font-size: 20px;"><b>Four Cheese Pizza  15 000 l.l</b></p>

            <p align="center" style="font-size: 20px;"><b>Vegeterian Pizza  15 000 l.l</b></p>

            <p align="center" style="font-size: 20px;"><b>Chicken BBQ. Pizza  16 000 l.l</b></p>

    </section>
    <!-- End Section -->


    <!-- Begin Section 4 Menu -->
    <section class="sections" id="SeaFood">
        <div class="col-md-4 div-right" >
            <img src="img/seaFood.jpg" class="menuImages">
        </div>
        <div class="col-md-4 div-right">
            <p style="font-size: 40px; text-align: center;"><i><b>Fish & SeaFood</b></i></p>
            
            <p align="center" style="font-size: 20px;"><b>Salmon with tomato saffron coulis  32 000 l.l</b></p>
            <p align="center">grilled gold salmon laying on light tomato saffron sauce and mashed potato<br> </p>

            <p align="center" style="font-size: 20px;"><b>Asian Hamour  25 000 l.l</b></p>
            <p align="center">baked hamour filet dressed with creamy leek sauce <br>
            served with mshed potato and sauteed vegetables </p>

            <p align="center" style="font-size: 20px;"><b>Grilled Citrus Fish  25 000 l.l</b></p>
            <p align="center">hamour fish, sauteed vegetables, broccoli, potato fingers<br> 
            served with citrus sauce</p>

            <p align="center" style="font-size: 20px;"><b>Grilled Salmon  33 000 l.l</b></p>
            <p align="center">Salmon sauteed vegetables baked potatoes<br> 
            served with lemon sauce</p>

            <p align="center" style="font-size: 20px;"><b>Thai Curry Shrimp  24 500 l.l</b></p>
            <p align="center">Shrimps, coconut ilk, green beans, cherry tomatoes,<br> 
            coriander, garlic, papays purse, served with basmtai rice</p>

            <p align="center" style="font-size: 20px;"><b>Grilled Jumbo Shrimps  42 000 l.l</b></p>
            <p align="center">grilled jumbo shrimps, potato fingers, shredded vegeables, mixed rice<br> 
            served with citrus sauce</p>
    </section>
    <!-- End Section -->


    <!-- Begin Section 3 Menu -->
    <section class="sections" id="BeefandPoultry">
        <div class="col-md-4 div-left">
            <img src="img/mashewe.jpg" class="menuImages">
        </div>
        <div class="col-md-4 div-left">
            <p style="font-size: 40px; text-align: center;"><i><b>Beef and Poultry</b></i></p>
            
            <p align="center" style="font-size: 20px;"><b>Giazed BBQ, Steak  29 500 l.l</b></p>
            <p align="center">200 gr of premium tenderloin with orange BBQ. sauce<br>  
            served with mashed potatoes and boiled vegetables</p>

            <p align="center" style="font-size: 20px;"><b>Chicken Picatta  22 500 l.l</b></p>
            <p align="center">grilled chicken breast tossed with creamy capers sauce<br> 
            served with potato wedges, carrots and broccoli</p>

            <p align="center" style="font-size: 20px;"><b>Beef Tenderloin  33 000 l.l</b></p>
            <p align="center">beef filet, mixed vegetables, mashed potatoes<br> 
            served with fresh mashroom sauce</p>

            <p align="center" style="font-size: 20px;"><b>Polo alfredo  22 000 l.l</b></p>
            <p align="center">grilled marinated chicken, assorted vegetables<br> 
            served with classic alfredo sauce</p>

            <p align="center" style="font-size: 20px;"><b>Tuscan chicken  24 500 l.l</b></p>
            <p align="center">chicken breast stuffed with sundried tomato, basil, mozzarella cheese,<br>
            with mashed potatoes with mustard, shredded vegetables, green veans<br> 
            served with mustard sauc</p>
        </div>
    </section>
    <!-- End Section -->


    <!-- Begin Section 4 Menu -->
    <section class="sections" id="ItalianCorner">
        <div class="col-md-4 div-right" >
            <img src="img/soups.jpg" class="menuImages">
        </div>
        <div class="col-md-4 div-right">
            <p style="font-size: 40px; text-align: center;"><i><b>Italian Corner</b></i></p>
            
            <p align="center" style="font-size: 20px;"><b>Penne Arbiatta  12 000 l.l</b></p>
            <p align="center">penne tomato sauce, hot pepper<br> 
            served with garlic bread and pesto bread </p>

            <p align="center" style="font-size: 20px;"><b>Penne For Cheese  16 000 l.l</b></p>
            <p align="center">pennem creamy sauce, cheddar cheese, emmanial cheese<br>
            parmesan, mozzarella cheese, basil</p>


            <p align="center" style="font-size: 20px;"><b>Tagliatelle Alfredo  20 000 l.l</b></p>
            <p align="center">tagliatelle, chicken breast, mashrooms<br/> 
            served with creamy sauce</p>
    </section>
    <!-- End Section -->


    <!-- Begin Section 3 Menu -->
    <section class="sections" >
        <div class="col-md-4 div-left">
            <img src="img/salads.jpg" class="menuImages">
        </div>
        <div class="col-md-4 div-left">
            <p style="font-size: 40px; text-align: center;"><i><b>Salads</b></i></p>
            
            <p align="center" style="font-size: 20px;"><b>Maxicn salad  15 000 l.l</b></p>
            <p align="center">sweet corn, black olive, avocado, red onion,<br> 
            tomato, green peper, cucumber<br> 
            served with house dressing</p>

            <p align="center" style="font-size: 20px;"><b>Italian Chicken Salad  15 000 l.l</b></p>
            <p align="center">marinated chicken breast, iceberg lettuce,<br>
            tomato, cucmber, green onion<br> 
            served with house dressing</p>

            <p align="center" style="font-size: 20px;"><b>Green Salad  12 000 l.l</b></p>
            <p align="center">mixed lettuce, cucumber,<br> 
            served with lemon dressing</p>

            <p align="center" style="font-size: 20px;"><b>Chiccken Caesar Salad  18 000 l.l</b></p>
            <p align="center">roman lettuce, grilled chicken strips,croutons, permesan<br> 
            served with ceaser dressing</p>

            <p align="center" style="font-size: 20px;"><b>Panache Salad  19 250 l.l</b></p>
            <p align="center">palmito, asparagus, artichoke, cherry tomato,<br>
            assorted lettuce, bay corn, avocado<br> 
            served with lemon mayo dressing</p>

            <p align="center" style="font-size: 20px;"><b>Greek Salad  14 500 l.l</b></p>
            <p align="center">mixed lettuce, fetta cheese, cherry tomato, cucumber, thyme, onion<br> 
            served with lemon mint dressing</p>
        </div>
    </section>
    <!-- End Section -->


    <!-- Begin Section 4 Menu -->
    <section class="sections">
        <div class="col-md-4 div-right" >
            <img src="img/soups.jpg" class="menuImages">
        </div>
        <div class="col-md-4 div-right">
            <p style="font-size: 40px; text-align: center;"><i><b>Soups</b></i></p>
            
            <p align="center" style="font-size: 20px;"><b>Lentil Soup  8 000 l.l</b></p>
            <p align="center">crushed lentil, onion, celery, coriander,<br> 
            served with croutons and lemon wedges </p>

            <p align="center" style="font-size: 20px;"><b>Creamy Soup  10 000 l.l</b></p>
            <p align="center">your choice of asparagus, brocoli or mashroom </p>

            <p align="center" style="font-size: 20px;"><b>Onion Soup  10 000 l.l</b></p>
            <p align="center">caramelized onion with beef stock<br> 
            and melted cheese on top</p>
    </section>
    <!-- End Section -->




    <!-- Begin Section 5 Menu -->
    <section class="sections">
        <div></div>
    </section>
    <!-- End Section -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/menu.js') }}"></script>

@endsection