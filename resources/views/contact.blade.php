@extends('layouts.app')
@section('content')

        <div id="AboutPage">
        	<div class="col-md-3 col-md-offset-8" id="aboutImage">
        		<img src="img/bridge.svg">
        		<ul id="social-media">
					<li><a href="https://www.facebook.com/bridgerestau/" id="facebook"></a></li>
					<li><a href="https://twitter.com/bridgerestau" id="twitter"></a></li>
					<li><a href="https://www.instagram.com/bridgerestau/" id="instagram"></a></li>
					<li><a href="http://www.bridgerest.net" id="network"></a></li>
				</ul>
				New Airport Road, first exit after the embassy of Kuwait (Towards Airport) <br>
				Tel: +961 1 453 777 / +961 1 453 888  <br>Mobile: +961 70 43 70 44 
        	</div>
        </div>
    
@endsection