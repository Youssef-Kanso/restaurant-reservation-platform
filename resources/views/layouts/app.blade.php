<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Bridge') }}</title>

    <!-- Styles -->
    <link rel="icon" href="img/b.png">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 
    <link href="{{ asset('css/extension.css') }}" rel="stylesheet">
        <link href="{{ asset('css/home.css') }}" rel="stylesheet">
        <link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">

</head>
<body style="font-family: calibri; ">
    <div id="app" >
        <nav class="navbar navbar-inverse navbar-fixed-top" style=" background-color:rgba(0, 0, 0, 0.8);">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Bridge
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-nav-left" >
                        &nbsp;
                    </ul>

                     <ul class="nav navbar-nav" style="  font-size: 17px; ">
                        <li class="navbar-center"><a href="{{ route('home') }}">Home</a></li>
                        <li class="navbar-center"><a href="{{ route('menu') }}">Menu</a></li>
                        @guest
                            <li class="navbar-center"><a href="{{ route('availablity') }}">MakeReservation</a></li>
                        @elseif(Auth::user()->admin == 1)
                            <li class="navbar-center"><a href="{{ route('reservations') }}">Reservations</a></li>
                        @else
                            <li class="navbar-center"><a href="{{ route('availablity') }}">MakeReservation</a></li>
                        @endguest
                        <li class="navbar-center"><a href="{{ route('events') }}">Events</a></li>
                        <li class="navbar-center"><a href="{{ route('contact') }}">Contact</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right navbar-nav-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" style="margin-right: 5%;">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    @if(Auth::user()->admin == 0)
                                <li class="navbar-center"><a href="{{ route('myResevations') }}">MyResrvations</a></li>
                                @endif

                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>
    <!-- Scripts -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
