@extends('layouts.app')
@section('content')
<div class="container" style="margin-top: 70px;">
    <div class="col-md-8 col-md-offset-2" >
        <form method="POST" action="{{URL::to('/')}}/checkAvailablity">
            {{ csrf_field() }}
            
            <!-- <input type="date" min='{{  date("Y-m-d") }}' name="date" @if(Session::has('date')) value="{{ Session::get('date') }}" disabled="disabled" @endif class="input" style="width: 49%; "> -->



        <div class="datepicker">
            <div id="title" class="label">1st May 2015</div>
            <div class="block action" onclick="addDay(1)">+</div>
            <div class="block action" onclick="addMonth(1)">+</div>
            <div class="block action" onclick="addYear(1)">+</div>
            <div ></div>
            <div class="block middle" id="day">1</div>
            <div class="block middle" id="month">May</div>
            <div class="block middle" id="year">15</div>
            <div ></div>
            <div class="block action" onclick="addDay(-1)">-</div>
            <div class="block action" onclick="addMonth(-1)">-</div>
            <div class="block action" onclick="addYear(-1)">-</div>
            <input type="hidden" id="date" name="date" value="">
        </div>

        <div class="timepicker">
          <div id="titleT" class="label">1st May 2015</div>
          <div class="block action" onclick="addHour(1)">+</div>
          <div class="block action" onclick="addMinutes(15)">+15</div>
          <div class="block noborder" >&nbsp;</div>
          <div ></div>
          <div class="block middle" id="hour">01</div>
          <div class="block middle" id="minutes">00</div>
          <div class="block middle action" id="ampm" onclick="toggleAMPM()">AM</div>
          <div ></div>
          <div class="block action" onclick="addHour(-1)">-</div>
          <div class="block action" onclick="addMinutes(-15)">-15</div>
          <div class="block noborder" >&nbsp;</div>
          <input type="hidden" id="time" name="time" value="">
        </div>

      
          <!--   <input type="time" @if(Session::has('time')) value="{{ Session::get('time') }}" disabled="disabled" @else value="12:00" @endif name="time" class="input"  style=" width: 39%">  -->
            <button type="submit" id="checkReservation" class="butn btn  btn-success">Check Reservation</button>
        </form>
    </div>
    @if(isset($reserved) && !empty($reserved))
        @php($tableNumber = array())
            @foreach($reserved as $reserve)
                @php($tableNumber[] = $reserve->table_number)
        @endforeach
    @endif
    @if(Session::has('reser'))
        @if(Session('reser') == 1)
            <div class="col-md-12 alert alert-success">
                    <h4><strong>Success!</strong> You have successfuly reserved the Table(Note: you can see your reservation details in myreservation page).</h4>
            </div>
            @endif
    @endif
    <div class="col-md-12 " >
        <div class="row"> 
    
        <form method="POST" action="{{URL::to('/')}}/Reservation ">
            {{ csrf_field() }}
            <div class="col-md-10 AvailablityBack" style="margin-top:-10px;">
                <div class="form-group product-chooser">
                    <div class="row " style="margin-top: 280px;">
                    
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"   @if(isset($tableNumber)) @if(in_array(1, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="1" >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"   @if(isset($tableNumber)) @if(in_array(2, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="2" >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>


                    </div>

                    <div class="row  co">
                    
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-1" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(3, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="3" >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" style=" padding-left:  " >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(4, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="4" >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(5, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="5"  >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(6, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="6" >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(7, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="7" >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        
                    </div>
                
                    <div class="row">
                    
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(8, $tableNumber)) class="btn disabled nar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="8">
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(9, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="9" >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  


                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(10, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="10"  >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(11, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="11"  >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(12, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="12"  >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " >
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(13, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="12" >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  


                        
                    </div>

                    <div class="row marT">
                    
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(14, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="14"  >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(15, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="15"  >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  


                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(16, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="16"  >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(17, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="17"  >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label"  @if(isset($tableNumber)) @if(in_array(18, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="18"  >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  

                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-0" >
                            <div class="product-chooser-item " style="text-align: center;">
                                <label name="label" @if(isset($tableNumber)) @if(in_array(19, $tableNumber)) class="btn disabled mar" @else class="btn mar" @endif @else class="btn mar"  @endif>
                                        <img src="img/tawleet.svg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-10 " alt="Mobile and Desktop" >
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <input type="radio" name="table" value="19"  >
                                        </div>
                                        <input type="hidden" name="hiddenDate" @if(Session::has('date')) value="{{ Session::get('date') }}" @endif>
                                        <input type="hidden" name="hiddenTime" @if(Session::has('time')) value="{{ Session::get('time') }}" @endif>
                                    <div class="clear"></div>
                                </label>
                            </div>
                        </div>  


                        
                    </div>

                </div>
            </div>
            <div class="col-md-3 col-md-offset-5">
                <input type="submit" class="btn btn-success"  id="resBtn" value="make reservation">
            </div>
        </form>
</div>
    </div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/reservation.js') }}"></script>
    <script src="{{ asset('js/timepicker.js') }}"></script>
    <script src="{{ asset('js/datepicker.js') }}"></script>


@endsection