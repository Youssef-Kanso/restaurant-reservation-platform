<?php

namespace Bridge;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected  $fillable = [
    	'category',
    	'food_name',
    	'price'
    ];
}
