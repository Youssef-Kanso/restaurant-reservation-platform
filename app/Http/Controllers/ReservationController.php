<?php

namespace Bridge\Http\Controllers;

use Bridge\Reservation;
use Bridge\User;
use Illuminate\Http\Request;
use Session;
use DB;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reservation');
    }

    public function make(Request $request)
    {
        $table_number = $request->get('table');
        $date = $request->get('hiddenDate');
        $time = $request->get('hiddenTime');
        return view('reservation')->with('table_n', $table_number)
                                ->with('date', $date)
                                ->with('time', $time);
    }

    public function admin()
    {
        $res = Reservation::where('date', '>=', date('Y-m-d'))->orderBy('date', 'DESC')->get();
        /*$res = DB::Table('reservations as r')->where('date', '>=', date('Y-m-d'))->orderBy('date', 'DESC')->get();*/        
        return view('reservations')->with('res', $res);
    }

    public function update(Request $request){

        Reservation::where('table_number', $request->get('id'))
                    ->update(['end_reservation' => 1]);

        // Reservation::create([
        //  'end_reservation' => 1,
        // ]);
        return redirect('reservations');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $res = Reservation::all()->where('user_id', '=', \Auth::user()->id);
        return view('myReservations')->with('result', $res);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Reservation::create([
            'time'=>$request->get('time'),
            'date'=>$request->get('date'), 
            'table_number'=>$request->get('table_number'), 
            'end_reservation'=> 0 ,
            'user_id'=> \Auth::user()->id 
        ]);
        $reser = 1;
        return redirect('availablity')->with('reser', $reser);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Bridge\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Bridge\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \Bridge\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
    }
}
