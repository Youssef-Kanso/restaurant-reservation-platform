<?php

namespace Bridge\Http\Controllers;

use Bridge\Order;
use Illuminate\Http\Request;
use Bridge\Reservation;
use Session;


class AvailablityController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('availablity');
    }

    public function check(Request $request)
    {
    	$reserved = array();
    	$date = $request->get('date');
    	Session::flash('date', $date);
    	$time = $request->get('time');
    	Session::flash('time', $time);
    	$reservedDate = Reservation::where('date','=',$date)->get();
    	if(isset($reservedDate) && !empty($reservedDate)){
    		foreach($reservedDate as $reserve){
    			if($reserve['end_reservation'] == 0){
    				$reserved[] = $reserve;
    			}
    		}
    	}  
        return view('availablity')->with('reserved', $reserved);
    }
    
}