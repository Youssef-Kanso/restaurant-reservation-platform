<?php

namespace Bridge;
Use Bridge\Table;
Use Bridge\User;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected  $fillable = [
    	'table_number', 
    	'time', 
    	'date',
        'end_reservation',
        'user_id'
    ];

    public function user() {
		return $this->belongsTo('Bridge\User');	
	}

	public function table() {
		return $this->hasMany('Bridge\table');	
	}
}

